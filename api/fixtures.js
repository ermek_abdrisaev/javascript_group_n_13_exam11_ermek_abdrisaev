const mongoose = require('mongoose');
const config = require('./config');
const Category = require('./models/Category');
const Product = require('./models/Product');
const User =require('./models/User');

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for(const coll of collections){
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [tech, foodAndDrinks, otherStuff] = await Category.create({
    category: 'Techs',
    description: 'Computers and technics'
  }, {
    category: 'FoodAndDrinks',
    description: 'Food and Drinks',
  },{
    category: 'OtherStuff',
    description: 'All other stuff'
  });

  const [alibaba, mahmudjon, emanuil] = await User.create({
    email: 'ali@ali.com',
    password: '123',
    name: 'Alibaba',
    token: 'hYYessvGFA4jl93AvU7Y8',
    phoneNumber: '550461010'
  }, {
    email: 'mahmud@alimahmud.gz',
    password: '123',
    name: 'Mahmudjon',
    token: 'f1U8k7QKMSjJ7VhWo2pHk',
    phoneNumber: '550461010'
  }, {
    email: 'emanuil@ema.gz',
    password: '123',
    name: 'Emanuil',
    token: 'lopc9jhC6qu_dM_eAO9XN',
    phoneNumber: '550461010'
  });

  await Product.create({
    category: tech,
    title: 'Asus ngrx300',
    price: 300,
    description: 'Office computer',
    image: 'asus.jpeg',
    user: alibaba
  }, {
    category: foodAndDrinks,
    title: 'Osh bazaar foods INC',
    price: 100,
    description: 'All foods and drincs from Osh bazaar',
    image: 'food.jpeg',
    user: mahmudjon
  },{
    category: otherStuff,
    title: 'Barakholka on lalafoo',
    price: 100,
    description: 'Plushkins heaven',
    image: 'macdonalds.png',
    user: emanuil
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));