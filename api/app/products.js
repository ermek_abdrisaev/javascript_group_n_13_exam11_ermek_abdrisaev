const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const Product = require("../models/Product");
const Category = require("../models/Category");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const query = {};

    if (req.query.filter === 'image') {
      query.image = {$ne: null};
    }

    const products = await Product.find(query).populate('category', 'title description');
    return res.send(products);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {

    const product = await Product.findOne({_id: req.params.id}).populate('user', 'displayName');
    if (!product) {
      return res.status(404).send({message: 'Not found'});
    }

    return res.send(product);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title || !req.body.price) {
      return res.status(400).send({message: 'Title and price are required'});
    }

    const productData = {
      category: req.body.category,
      title: req.body.title,
      price: parseFloat(req.body.price),
      description: req.body.description,
      user: req.body.user,
      image: null,
    };

    if (req.file) {
      productData.image = req.file.filename;
    }

    const product = new Product(productData);
    console.log(product);
    await product.save();

    return res.send(
      {message: 'Created new product', id: product._id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;