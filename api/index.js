const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const users = require('./app/users');
const products = require('./app/products');
const categories = require('./app/categories');

const config = require('./config');
const app = express();

const port = 8080;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use(express.static('public'));
app.use('/users', users);
app.use('/products', products);
app.use('/categories', categories);


const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  app.listen(port, () =>{
    console.log(`Server started on ${port} post`);
  });

  process.on('exit', () =>{
    mongoose.disconnect();
  });
};

run().catch(e => console.error(e));