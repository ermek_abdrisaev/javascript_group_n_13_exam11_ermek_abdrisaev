import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { ProductData } from '../../models/product.model';
import { createProductRequest } from '../../store/products.actions';

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.sass']
})
export class NewItemComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable< boolean>;
  error: Observable<string | null>;

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.products.createLoading);
    this.error = store.select(state => state.products.createError);
  }

  ngOnInit(): void {
  }

  onSubmit(){
    const productData: ProductData = this.form.value;
    this.store.dispatch(createProductRequest({productData}));
  }

}
