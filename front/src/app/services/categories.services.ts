import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiCategoryData, CategoryData } from '../models/categories.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CategoriesServices {
  constructor(private http: HttpClient){}

  getCategory(id: string){
    return this.http.get<ApiCategoryData[]>(environment.apiUrl + '/categories/' + id).pipe(
      map(result =>{
        console.log(result);
        return result;
      })
    )
  }

  createCategory(categoryData: CategoryData){
    const formData = new FormData();
    formData.append('category', categoryData.category);
    formData.append('description', categoryData.description);

    return this.http.post(environment.apiUrl + '/categories', formData);
  }


}
