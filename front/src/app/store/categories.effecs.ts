import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { CategoriesServices } from '../services/categories.services';
import { Router } from '@angular/router';
import {
  createCategoryFailure,
  createCategoryRequest, createCategorySuccess,
  fetchCategoryFailure,
  fetchCategoryRequest,
  fetchCategorySuccess
} from './categories.actions';
import { map } from 'rxjs/operators';
import { catchError, mergeMap, of, tap } from 'rxjs';

@Injectable()
export class CategoriesEffects {

  fetchCategory = createEffect(() => this.actions.pipe(
    ofType(fetchCategoryRequest),
    mergeMap(id => this.categoriesService.getCategory(id.id).pipe(
      map(categories => fetchCategorySuccess({categories})),
      catchError(() => of(fetchCategoryFailure({error: 'Something went wrong!!!'})))
    ))
  ));

  createCategory = createEffect(() => this.actions.pipe(
      ofType(createCategoryRequest),
      mergeMap(({categoryData}) => this.categoriesService.createCategory(categoryData).pipe(
          map(() => createCategorySuccess()),
          catchError(() => {
            return of(createCategoryFailure({
              error: 'Wrong data!'
            }));
          })
        )
      )
    )
  );


  constructor(
    private actions: Actions,
    private categoriesService: CategoriesServices,
    private router: Router
  ) {
  }
}
