import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createProductFailure,
  createProductRequest, createProductSuccess, fetchProductFailure, fetchProductRequest,
  fetchProductsFailure,
  fetchProductsRequest,
  fetchProductsSuccess, fetchProductSuccess
} from './products.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProductsService } from '../services/products.service';
import { Router } from '@angular/router';

@Injectable()
export class ProductsEffects {
  fetchProducts = createEffect(() => this.actions.pipe(
    ofType(fetchProductsRequest),
    mergeMap(() => this.productsService.getProducts().pipe(
      map(products => fetchProductsSuccess({products})),
      catchError(() => of(fetchProductsFailure({error: "Something went wrong"})))
    ))
  ));

  fetchProduct = createEffect(() => this.actions.pipe(
    ofType(fetchProductRequest),
    mergeMap(id => this.productsService.getProduct(id.id).pipe(
      map(product => fetchProductSuccess({product})),
      catchError(() => of(fetchProductFailure({error: 'Something went wrong'})))
    ))
  ));

  createProduct = createEffect(() => this.actions.pipe(
    ofType(createProductRequest),
    mergeMap(({productData}) => this.productsService.createProduct(productData)
      .pipe(
        map(() => createProductSuccess()),
        tap(() => {
          return this.router.navigate(['/']);
        }),
        catchError(() => {
          return of(createProductFailure({
            error: 'Wrong data'
          }));
        })
      )
    )
  ));

  constructor(
    private actions: Actions,
    private productsService: ProductsService,
    private router: Router
  ){}
}
