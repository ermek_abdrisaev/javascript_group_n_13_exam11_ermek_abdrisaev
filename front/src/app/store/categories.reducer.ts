import { CategoryState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createCategoryFailure,
  createCategoryRequest, createCategorySuccess,
  fetchCategoryFailure,
  fetchCategoryRequest,
  fetchCategorySuccess
} from './categories.actions';

const initialState: CategoryState = {
  categories: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};

export const categoryReducer = createReducer(
  initialState,
  on(fetchCategoryRequest, state => ({...state, fetchLoading: true})),
  on(fetchCategorySuccess, (state, {categories}) => ({...state, fetchLoading: false, categories})),
  on(fetchCategoryFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createCategoryRequest, state => ({...state, createLoading: true})),
  on(createCategorySuccess, state => ({...state, createLoading: false})),
  on(createCategoryFailure, (state, {error}) => ({...state, createLoading: false, createError: error}))
)
