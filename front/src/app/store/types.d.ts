import { LoginError, RegisterError, User } from '../models/user.model';
import { Product } from '../models/product.model';
import { Category } from '../models/categories.model';

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
};

export type ProductsState = {
  products: Product[],
  product: Product | null,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type CategoryState = {
  categories: Category[];
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
}

export type AppState = {
  users: UsersState,
  products: ProductsState,
  categories: CategoryState
};
