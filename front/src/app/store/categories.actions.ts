import { createAction, props } from '@ngrx/store';
import { Category, CategoryData } from '../models/categories.model';

export const fetchCategoryRequest = createAction(
  '[Category] Fetch Request',
  props<{ id: string }>()
);
export const fetchCategorySuccess = createAction(
  '[Category] Fetch Success',
  props<{ categories: Category[] }>()
);
export const fetchCategoryFailure = createAction(
  '[Category] Fetch Failure',
  props<{ error: string }>()
);

export const createCategoryRequest = createAction(
  '[Category] Create Request',
  props<{ categoryData: CategoryData }>()
);
export const createCategorySuccess = createAction(
  '[Category] Create Success'
);
export const createCategoryFailure = createAction(
  '[Category] Create Failure',
  props<{ error: string }>()
);
