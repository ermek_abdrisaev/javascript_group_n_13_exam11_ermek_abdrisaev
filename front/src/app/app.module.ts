import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { CenteredCardComponent } from './ui/centered-card/centered-card.component';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';
import { MatMenuModule } from '@angular/material/menu';
import { ProductsComponent } from './products/products.component';
import { usersReducer } from './store/users.reucer';
import { UsersEffects } from './store/users.effects';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { productsReducer } from './store/products.reducer';
import { ProductsEffects } from './store/produts.effects';
import { localStorageSync } from 'ngrx-store-localstorage';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NewItemComponent } from './pages/new-item/new-item.component';
import { ImagePipe } from './pipes/image.pipe';
import { ProductComponent } from './products/product/product.component';

const localStorageSyncReducer = (reducer: ActionReducer<any>) =>{
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true,
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer]


@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    RegisterComponent,
    LoginComponent,
    CenteredCardComponent,
    ProductsComponent,
    FileInputComponent,
    CenteredCardComponent,
    NewItemComponent,
    ImagePipe,
    ProductComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    FormsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    StoreModule.forRoot({
      users: usersReducer,
      products: productsReducer,
    }, {metaReducers}),
    EffectsModule.forRoot([UsersEffects, ProductsEffects]),
    MatInputModule,
    MatCardModule,
    FlexModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
