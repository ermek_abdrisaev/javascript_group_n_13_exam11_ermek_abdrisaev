import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AppState } from '../store/types';
import { Store } from '@ngrx/store';
import { Product } from '../models/product.model';
import { fetchProductsRequest } from '../store/products.actions';
import { Category } from '../models/categories.model';
import { fetchCategoryRequest } from '../store/categories.actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.sass']
})
export class ProductsComponent implements OnInit {
  products: Observable<Product[]>
  loading: Observable<boolean>
  error: Observable<null | string>

  category: Observable<Category[]>;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.products = store.select(state => state.products.products);
    this.loading = store.select(state => state.products.fetchLoading);
    this.error = store.select(state => state.products.fetchError);

    this.category = store.select(state => state.categories.categories);
    this.loading = store.select(state => state.categories.fetchLoading);
    this.error = store.select(state => state.categories.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchProductsRequest());
    this.route.params.subscribe(category =>{
      this.store.dispatch(fetchCategoryRequest({id: category['id']}))
    });
  }
}
