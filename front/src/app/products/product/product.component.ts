import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiProductData, Product } from '../../models/product.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { fetchProductRequest } from '../../store/products.actions';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass']
})
export class ProductComponent implements OnInit {
  product: Observable<Product | null>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  productId!: ApiProductData;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.product = store.select(state => state.products.product);
    this.loading = store.select(state => state.products.fetchLoading);
    this.error = store.select(state => state.products.fetchError);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch((fetchProductRequest({id: params['id']})));
    })
    this.product.subscribe(product =>{
      this.productId = <ApiProductData>product;
    })
  }

}
