import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { ProductsComponent } from './products/products.component';
import { NewItemComponent } from './pages/new-item/new-item.component';
import { ProductComponent } from './products/product/product.component';

const routes: Routes = [
  {path: '', component: ProductsComponent},
  {path: 'new-item', component: NewItemComponent},
  {path: 'products/:id', component: ProductComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
