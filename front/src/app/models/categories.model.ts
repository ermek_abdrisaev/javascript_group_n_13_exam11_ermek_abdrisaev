export class Category {
  constructor(
    public _id: string,
    public category: string,
    public description: string,
  ){}
}

export interface CategoryData {
  category: string;
  description: string;
}

export interface ApiCategoryData{
  _id: string,
  category: string,
  description: string,
}
