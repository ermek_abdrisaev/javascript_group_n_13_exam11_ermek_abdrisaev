export class Product {
  constructor(
    public _id: string,
    public title: string,
    public price: number,
    public description: string,
    public image: string,
    public displayName: string,
    public phoneNumber: string,
  ) {}
}

export interface ProductData {
  [key: string]: any;
  title: string;
  price: number;
  description: string;
  image: File | null;
  displayName: string,
  phoneNumber: string
}

export interface ApiProductData {
  _id: string,
  title: string,
  price: number,
  description: string,
  image: string,
  displayName: string,
  phoneNumber: string,
}
